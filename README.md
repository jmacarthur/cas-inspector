# CAS Tool

This is a simple tool designed to help people working with
content-addressable stores as used by BuildStream. It requires the
Google Protocol Buffer libraries - see the later requirements section
for details.

There are two commands, 'inspect' and 'extract'. Both take a text
SHA256 hash as arguments.

'inspect' will try and figure out what kind of object the hash
represents. It understands directory, action and command objects and
if none of those match, it will report that the hash points to a plain
file. This process is not completely reliable. To be fully reliable,
you need context with the hash about what kind of object it is.

'extract' will extract a directory or file, pointed to by the hash, to
a pathname supplied as the second argument.

By default, this looks in "~/.cache/buildstream/artifacts/cas" for the
CAS-based storage. This is the only BuildStream-specific part of the
tool. You can pass in another directory using the --casdir argument or
the CASDIR environment variable. The directory specified should at
least contain an 'objects' directory.

## Requirements

This tool is meant to use a content-addressable store which uses the
Google Protocol for remote execution, used by BuildStream and Bazel.

This requires google.devtools.remoteexecution and google.protobuf,
both of which can be found in the google/ directory of BuildStream
(juerg/cascache branch, not merged at the time of writing).

For more details on the remote execution API, Google have a design
document here:
https://docs.google.com/document/d/1AaGk7fOPByEvpAbqeXIyE8HX_A3_axxNnvroblTZ_6s
